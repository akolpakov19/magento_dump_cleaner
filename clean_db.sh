#!/bin/bash

echo $1
FILE_DUMP=$1
sed -i "/INSERT INTO \`\(adminnotification_inbox\|aw_core_logger\|dataflow_batch_export\|dataflow_batch_import\|log_customer\|log_quote\|log_summary\|log_summary_type\|log_url\|log_url_info\|log_visitor\|log_visitor_info\|log_visitor_online\|index_event\|report_event\|report_compared_product_index\|report_viewed_product_index\|catalog_compare_item\|catalogindex_aggregation\|catalogindex_aggregation_tag\|catalogindex_aggregation_to_tag\)\`/d" $FILE_DUMP
